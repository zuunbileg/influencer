export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Delivering Smart LifeStyle",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      {
        hid: "og:image",
        property: "og:image",
        content: "https://i.imgur.com/LVwu6sO.png",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  server: {
    port: 8000, // default: 3000
    timing: false,
    // https: {
    //   key: fs.readFileSync(path.resolve(__dirname, 'localhost.key')),
    //   cert: fs.readFileSync(path.resolve(__dirname, 'localhost.crt'))
    // }
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "./plugins/infinity.js", ssr: false },
    { src: "~/plugins/fbchat.js", ssr: false },
    { src: "~/plugins/validation.js" },
    { src: "~/plugins/qr.js", ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/moment",
    "@nuxtjs/google-fonts",
    "@nuxtjs/tailwindcss",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/toast",
    "@nuxtjs/auth-next",
    "@nuxtjs/gtm",
  ],

  gtm: {
    id: "GTM-NCJZMJL",
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  axios: {
    // baseURL: "http://localhost:3000",
    baseURL: "https://api.topsale.mn",
  },

  googleFonts: {
    families: {
      Roboto: true,
      Helvetica: true,
      "Helvetica Neue": true,
    },
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: "token",
          maxAge: 3600 * 60 * 24 * 30 * 12,
          global: true,
          type: "Bearer",
        },
        endpoints: {
          login: {
            url: "/api/auth/login",
            method: "post",
            propertyName: "token",
          },
          logout: { url: "/api/auth/logout", method: "post" },
          user: { url: "/api/auth/me", method: "post", propertyName: "user" },
        },
        tokenRequired: true,
        tokenType: "Bearer",
        globalToken: true,
      },
      facebook: {
        endpoints: {
          userInfo:
            "https://graph.facebook.com/v6.0/me?fields=id,email,name,picture{url}",
        },
        clientId: "1182711955627744",
        scope: ["public_profile", "email"],
      },
    },
    redirect: {
      login: "/login",
      logout: "/",
      callback: "/login",
      home: "/",
    },
  },

  toast: {
    position: "bottom-center",
    duration: 3000,
    register: [
      // Register custom toasts
      {
        name: "my-error",
        message: "Oops...Something went wrong",
        options: {
          type: "error",
        },
      },
    ],
  },
};
