import Vue from 'vue'
import VueQrcode from '@chenfengyuan/vue-qrcode';

// Register the Vue component
Vue.component('qrcode-vue', VueQrcode)