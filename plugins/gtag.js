import Vue from "vue";
import VueGtag from "vue-gtag";

Vue.use(VueGtag, {
  config: { id: "G-K6HZJQYM7C" },
  _setDomainName: 'none'
})