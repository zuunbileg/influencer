exports.tables = [
  {
    id: 1,
    category: "huuhdiinhuvtsas",
    head: [{ kor: "Солонгос", euro: "Европ", us: " Америк", mn: "Монгол" }],
    datakor: [
      {
        XS: "70A",
        S: "75A",
        M: "80B",
        L: "85C",
        XL: "90D",
      },
    ],
    dataeuro: [{ XS: "70A", S: "75A", M: "80B", L: "85C", XL: "90D" }],
    dataus: [
      {
        XS: "70A",
        S: "75A",
        M: "80B",
        L: "85C",
        XL: "90D",
      },
    ],
    datamn: [
      {
        XS: "70A",
        S: "75A",
        M: "80B",
        L: "85C",
        XL: "90D",
      },
    ],
  },
];
