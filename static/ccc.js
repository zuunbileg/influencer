exports.city = [
  {
    id: "1",
    name: "Улаанбаатар",
    parent: null,
  },
];

exports.district = [
  {
    id: "1",
    name: "Багануур дүүрэг",
    parent: 1,
  },

  {
    id: "2",
    name: "Багахангай дүүрэг",
    parent: 1,
  },

  {
    id: "3",
    name: "Баянгол дүүрэг",
    parent: 1,
  },

  {
    id: "4",
    name: "Баянзүрх дүүрэг",
    parent: 1,
  },

  {
    id: "5",
    name: "Налайх дүүрэг",
    parent: 1,
  },
  {
    id: "6",
    name: "Сонгинохайрхан дүүрэг",
    parent: 1,
  },

  {
    id: "7",
    name: "Сүхбаатар дүүрэг",
    parent: 1,
  },

  {
    id: "8",
    name: "Хан-Уул дүүрэг",
    parent: 1,
  },

  {
    id: "9",
    name: "Чингэлтэй дүүрэг",
    parent: 1,
  },
];

exports.section = [
  {
    id: 1,
    name: "1-р хороо",
    parent: 1,
  },
  {
    id: 5,
    name: "5-р хороо",
    parent: 1,
  },
  {
    id: 6,
    name: "1-р хороо",
    parent: 2,
  },
  {
    id: 7,
    name: "2-р хороо",
    parent: 2,
  },
  {
    id: 8,
    name: "1-р хороо",
    parent: 3,
  },
  {
    id: 9,
    name: "2-р хороо",
    parent: 3,
  },
  {
    id: 10,
    name: "3-р хороо",
    parent: 3,
  },
  {
    id: 11,
    name: "4-р хороо",
    parent: 3,
  },
  {
    id: 12,
    name: "5-р хороо",
    parent: 3,
  },
  {
    id: 13,
    name: "6-р хороо",
    parent: 3,
  },
  {
    id: 14,
    name: "7-р хороо",
    parent: 3,
  },
  {
    id: 15,
    name: "8-р хороо",
    parent: 3,
  },
  {
    id: 16,
    name: "9-р хороо",
    parent: 3,
  },
  {
    id: 17,
    name: "10-р хороо",
    parent: 3,
  },
  {
    id: 2,
    name: "2-р хороо",
    parent: 1,
  },
  {
    id: 19,
    name: "12-р хороо",
    parent: 3,
  },
  {
    id: 20,
    name: "13-р хороо",
    parent: 3,
  },
  {
    id: 18,
    name: "11-р хороо",
    parent: 3,
  },
  {
    id: 4,
    name: "4-р хороо",
    parent: 1,
  },
  {
    id: 23,
    name: "16-р хороо",
    parent: 3,
  },
  {
    id: 24,
    name: "17-р хороо",
    parent: 3,
  },
  {
    id: 25,
    name: "18-р хороо",
    parent: 3,
  },
  {
    id: 26,
    name: "19-р хороо",
    parent: 3,
  },
  {
    id: 27,
    name: "20-р хороо",
    parent: 3,
  },
  {
    id: 28,
    name: "21-р хороо",
    parent: 3,
  },
  {
    id: 29,
    name: "22-р хороо",
    parent: 3,
  },
  {
    id: 30,
    name: "23-р хороо",
    parent: 3,
  },
  {
    id: 31,
    name: "24-р хороо",
    parent: 3,
  },
  {
    id: 3,
    name: "3-р хороо",
    parent: 1,
  },
  {
    id: 33,
    name: "1-р хороо",
    parent: 4,
  },
  {
    id: 34,
    name: "2-р хороо",
    parent: 4,
  },
  {
    id: 35,
    name: "3-р хороо",
    parent: 4,
  },
  {
    id: 36,
    name: "4-р хороо",
    parent: 4,
  },
  {
    id: 37,
    name: "5-р хороо",
    parent: 4,
  },
  {
    id: 21,
    name: "14-р хороо",
    parent: 3,
  },
  {
    id: 39,
    name: "7-р хороо",
    parent: 4,
  },
  {
    id: 32,
    name: "25-р хороо",
    parent: 3,
  },
  {
    id: 41,
    name: "9-р хороо",
    parent: 4,
  },
  {
    id: 42,
    name: "10-р хороо",
    parent: 4,
  },
  {
    id: 43,
    name: "11-р хороо",
    parent: 4,
  },
  {
    id: 44,
    name: "12-р хороо",
    parent: 4,
  },
  {
    id: 45,
    name: "13-р хороо",
    parent: 4,
  },
  {
    id: 46,
    name: "14-р хороо",
    parent: 4,
  },
  {
    id: 47,
    name: "15-р хороо",
    parent: 4,
  },
  {
    id: 48,
    name: "16-р хороо",
    parent: 4,
  },
  {
    id: 49,
    name: "17-р хороо",
    parent: 4,
  },
  {
    id: 50,
    name: "18-р хороо",
    parent: 4,
  },
  {
    id: 51,
    name: "19-р хороо",
    parent: 4,
  },
  {
    id: 52,
    name: "20-р хороо",
    parent: 4,
  },
  {
    id: 53,
    name: "21-р хороо",
    parent: 4,
  },
  {
    id: 54,
    name: "22-р хороо",
    parent: 4,
  },
  {
    id: 55,
    name: "23-р хороо",
    parent: 4,
  },
  {
    id: 56,
    name: "24-р хороо",
    parent: 4,
  },
  {
    id: 57,
    name: "25-р хороо",
    parent: 4,
  },
  {
    id: 58,
    name: "26-р хороо",
    parent: 4,
  },
  {
    id: 59,
    name: "27-р хороо",
    parent: 4,
  },
  {
    id: 60,
    name: "28-р хороо",
    parent: 4,
  },
  {
    id: 22,
    name: "15-р хороо",
    parent: 3,
  },
  {
    id: 62,
    name: "2-р хороо",
    parent: 5,
  },
  {
    id: 40,
    name: "8-р хороо",
    parent: 4,
  },
  {
    id: 64,
    name: "4-р хороо",
    parent: 5,
  },
  {
    id: 65,
    name: "5-р хороо",
    parent: 5,
  },
  {
    id: 66,
    name: "6-р хороо",
    parent: 5,
  },
  {
    id: 67,
    name: "7-р хороо",
    parent: 5,
  },
  {
    id: 68,
    name: "1-р хороо",
    parent: 6,
  },
  {
    id: 69,
    name: "2-р хороо",
    parent: 6,
  },
  {
    id: 70,
    name: "3-р хороо",
    parent: 6,
  },
  {
    id: 71,
    name: "4-р хороо",
    parent: 6,
  },
  {
    id: 72,
    name: "5-р хороо",
    parent: 6,
  },
  {
    id: 73,
    name: "6-р хороо",
    parent: 6,
  },
  {
    id: 74,
    name: "7-р хороо",
    parent: 6,
  },
  {
    id: 63,
    name: "3-р хороо",
    parent: 5,
  },
  {
    id: 76,
    name: "9-р хороо",
    parent: 6,
  },
  {
    id: 77,
    name: "10-р хороо",
    parent: 6,
  },
  {
    id: 78,
    name: "11-р хороо",
    parent: 6,
  },
  {
    id: 61,
    name: "1-р хороо",
    parent: 5,
  },
  {
    id: 80,
    name: "13-р хороо",
    parent: 6,
  },
  {
    id: 81,
    name: "14-р хороо",
    parent: 6,
  },
  {
    id: 82,
    name: "15-р хороо",
    parent: 6,
  },
  {
    id: 83,
    name: "16-р хороо",
    parent: 6,
  },
  {
    id: 84,
    name: "17-р хороо",
    parent: 6,
  },
  {
    id: 85,
    name: "18-р хороо",
    parent: 6,
  },
  {
    id: 38,
    name: "6-р хороо",
    parent: 4,
  },
  {
    id: 87,
    name: "20-р хороо",
    parent: 6,
  },
  {
    id: 79,
    name: "12-р хороо",
    parent: 6,
  },
  {
    id: 89,
    name: "22-р хороо",
    parent: 6,
  },
  {
    id: 90,
    name: "23-р хороо",
    parent: 6,
  },
  {
    id: 91,
    name: "24-р хороо",
    parent: 6,
  },
  {
    id: 92,
    name: "25-р хороо",
    parent: 6,
  },
  {
    id: 93,
    name: "26-р хороо",
    parent: 6,
  },
  {
    id: 75,
    name: "8-р хороо",
    parent: 6,
  },
  {
    id: 95,
    name: "28-р хороо",
    parent: 6,
  },
  {
    id: 96,
    name: "29-р хороо",
    parent: 6,
  },
  {
    id: 97,
    name: "30-р хороо",
    parent: 6,
  },
  {
    id: 94,
    name: "27-р хороо",
    parent: 6,
  },
  {
    id: 98,
    name: "31-р хороо",
    parent: 6,
  },
  {
    id: 99,
    name: "32-р хороо",
    parent: 6,
  },
  {
    id: 100,
    name: "33-р хороо",
    parent: 6,
  },
  {
    id: 86,
    name: "19-р хороо",
    parent: 6,
  },
  {
    id: 103,
    name: "36-р хороо",
    parent: 6,
  },
  {
    id: 101,
    name: "34-р хороо",
    parent: 6,
  },
  {
    id: 105,
    name: "38-р хороо",
    parent: 6,
  },
  {
    id: 106,
    name: "39-р хороо",
    parent: 6,
  },
  {
    id: 107,
    name: "40-р хороо",
    parent: 6,
  },
  {
    id: 108,
    name: "41-р хороо",
    parent: 6,
  },
  {
    id: 109,
    name: "42-р хороо",
    parent: 6,
  },
  {
    id: 110,
    name: "43-р хороо",
    parent: 6,
  },
  {
    id: 88,
    name: "21-р хороо",
    parent: 6,
  },
  {
    id: 111,
    name: "1-р хороо",
    parent: 7,
  },
  {
    id: 112,
    name: "2-р хороо",
    parent: 7,
  },
  {
    id: 114,
    name: "4-р хороо",
    parent: 7,
  },
  {
    id: 115,
    name: "5-р хороо",
    parent: 7,
  },
  {
    id: 116,
    name: "6-р хороо",
    parent: 7,
  },
  {
    id: 117,
    name: "7-р хороо",
    parent: 7,
  },
  {
    id: 118,
    name: "8-р хороо",
    parent: 7,
  },
  {
    id: 119,
    name: "9-р хороо",
    parent: 7,
  },
  {
    id: 120,
    name: "10-р хороо",
    parent: 7,
  },
  {
    id: 121,
    name: "11-р хороо",
    parent: 7,
  },
  {
    id: 113,
    name: "3-р хороо",
    parent: 7,
  },
  {
    id: 123,
    name: "13-р хороо",
    parent: 7,
  },
  {
    id: 124,
    name: "14-р хороо",
    parent: 7,
  },
  {
    id: 125,
    name: "15-р хороо",
    parent: 7,
  },
  {
    id: 126,
    name: "16-р хороо",
    parent: 7,
  },
  {
    id: 127,
    name: "17-р хороо",
    parent: 7,
  },
  {
    id: 128,
    name: "18-р хороо",
    parent: 7,
  },
  {
    id: 102,
    name: "35-р хороо",
    parent: 6,
  },
  {
    id: 130,
    name: "20-р хороо",
    parent: 7,
  },
  {
    id: 131,
    name: "1-р хороо",
    parent: 8,
  },
  {
    id: 132,
    name: "2-р хороо",
    parent: 8,
  },
  {
    id: 133,
    name: "3-р хороо",
    parent: 8,
  },
  {
    id: 129,
    name: "19-р хороо",
    parent: 7,
  },
  {
    id: 135,
    name: "5-р хороо",
    parent: 8,
  },
  {
    id: 136,
    name: "6-р хороо",
    parent: 8,
  },
  {
    id: 134,
    name: "4-р хороо",
    parent: 8,
  },
  {
    id: 138,
    name: "8-р хороо",
    parent: 8,
  },
  {
    id: 139,
    name: "9-р хороо",
    parent: 8,
  },
  {
    id: 140,
    name: "10-р хороо",
    parent: 8,
  },
  {
    id: 141,
    name: "11-р хороо",
    parent: 8,
  },
  {
    id: 142,
    name: "12-р хороо",
    parent: 8,
  },
  {
    id: 143,
    name: "13-р хороо",
    parent: 8,
  },
  {
    id: 144,
    name: "14-р хороо",
    parent: 8,
  },
  {
    id: 145,
    name: "15-р хороо",
    parent: 8,
  },
  {
    id: 104,
    name: "37-р хороо",
    parent: 6,
  },
  {
    id: 147,
    name: "17-р хороо",
    parent: 8,
  },
  {
    id: 148,
    name: "18-р хороо",
    parent: 8,
  },
  {
    id: 149,
    name: "19-р хороо",
    parent: 8,
  },
  {
    id: 150,
    name: "20-р хороо",
    parent: 8,
  },
  {
    id: 151,
    name: "21-р хороо",
    parent: 8,
  },
  {
    id: 152,
    name: "1-р хороо",
    parent: 9,
  },
  {
    id: 153,
    name: "2-р хороо",
    parent: 9,
  },
  {
    id: 154,
    name: "3-р хороо",
    parent: 9,
  },
  {
    id: 155,
    name: "4-р хороо",
    parent: 9,
  },
  {
    id: 156,
    name: "5-р хороо",
    parent: 9,
  },
  {
    id: 157,
    name: "6-р хороо",
    parent: 9,
  },
  {
    id: 158,
    name: "7-р хороо",
    parent: 9,
  },
  {
    id: 159,
    name: "8-р хороо",
    parent: 9,
  },
  {
    id: 160,
    name: "9-р хороо",
    parent: 9,
  },
  {
    id: 161,
    name: "10-р хороо",
    parent: 9,
  },
  {
    id: 162,
    name: "11-р хороо",
    parent: 9,
  },
  {
    id: 163,
    name: "12-р хороо",
    parent: 9,
  },
  {
    id: 164,
    name: "13-р хороо",
    parent: 9,
  },
  {
    id: 165,
    name: "14-р хороо",
    parent: 9,
  },
  {
    id: 166,
    name: "15-р хороо",
    parent: 9,
  },
  {
    id: 167,
    name: "16-р хороо",
    parent: 9,
  },
  {
    id: 137,
    name: "7-р хороо",
    parent: 8,
  },
  {
    id: 169,
    name: "18-р хороо",
    parent: 9,
  },
  {
    id: 170,
    name: "19-р хороо",
    parent: 9,
  },
  {
    id: 146,
    name: "16-р хороо",
    parent: 8,
  },
  {
    id: 168,
    name: "17-р хороо",
    parent: 9,
  },
  {
    id: 122,
    name: "12-р хороо",
    parent: 7,
  },
];
