export const state = () => ({
    carts: {},
    subtotal: 0,
    you_save: 0,
    total: 0
  })


  export const getters = {
    allCart: (state) => state.carts,
    subtotal: (state) => state.subtotal,
    you_save: (state) => state.you_save,
    total: (state) => state.total,
  }

  export const actions = {
    async addCart({commit}, data) {
        let cart = await this.$axios.put('/api/auth/cart/edit', data);
        this.$toast.success("Таны сагсанд амжилттай нэмэгдлээ. ");
        commit("setCart", cart.data)
    },
    async createCart({commit}, data) {
        let cart = await this.$axios.post('/api/auth/cart/new', data);
        commit("setCart", cart.data)
    },
    async findCart({commit}, data) {
        let cart = await this.$axios.post("/api/auth/cart/find", data);
        commit("setCart", cart.data)
    },
    async removeProduct({commit}, data) {
      let cart = await this.$axios.put('/api/auth/cart/edit', data);
      this.$toast.success("Таны сагснаас амжилттай хасагдлаа")
      commit("setCart", data)
    },
    async changeQty({commit}, data) {
      let cart = await this.$axios.put('/api/auth/cart/edit', data);
      commit("setCart", data)
    }
  }

  export const mutations = {
    setCart: (state, cart) => {
      state.carts = cart
      state.subtotal = 0
      state.you_save = 0
      for (let product of state.carts.products) {
        for(let varient of product.product.variants) {
          if (varient._id === product.varient_id) { 
            state.subtotal = state.subtotal + varient.selling_price * product.qty
            state.you_save = state.you_save + (product.product.price - varient.selling_price) * product.qty 
          }
        }
      }
      state.total = state.subtotal

    },
  }